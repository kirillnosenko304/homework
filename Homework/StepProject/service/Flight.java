package service;


import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;

public class Flight implements Serializable {
    public Flight(int flightId, String destination, int date, int time, int availableSeats) {
        this.flightId = flightId;
        this.destination = destination;
        this.date = LocalDate.ofEpochDay(date);
        this.time = LocalTime.ofSecondOfDay(time);
        this.availableSeats = availableSeats;

    }

    @Override
    public String toString() {
        return "Flight{" +
                "flightId=" + flightId +
                ", destination='" + destination + '\'' +
                ", date=" + date +
                ", time=" + time +
                ", availableSeats=" + availableSeats +
                '}';
    }

    private int flightId;
    private String destination;
    private LocalDate date;
    private LocalTime time;
    private int availableSeats;

    public int getFlightId(int flightId) {
        return flightId;
    }

    public void setFlightId(int flightId) {
        this.flightId = flightId;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public int getAvailableSeats() {
        return availableSeats;
    }

    public void setAvailableSeats(int availableSeats) {
        this.availableSeats = availableSeats;
    }

}
