package dao;

import service.Flight;

import java.util.List;

public interface FlightDao {
    List<Flight> getAllFlights();
    Flight getFlightById(int id);
    void addFlight(Flight flight);
    void updateFlight(Flight flight);
    void deleteFlight(int id);
}