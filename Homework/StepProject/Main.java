import controller.FileFlightDao;
import dao.FlightDao;
import service.Flight;

public class Main {
    public static void main(String[] args) {
        Flight flight = new Flight(2, "wads" , 23, 23 ,42);
        Flight flight1 = new Flight(22, "wadswadsdawdsa" , 233, 23 ,44);
        FileFlightDao fileFlightDao = new FileFlightDao();

        fileFlightDao.addFlight(flight);
        fileFlightDao.addFlight(flight1);

        fileFlightDao.loadFlightFromFile();
        System.out.println(fileFlightDao.loadFlightFromFile());
    }
}
