package controller;

import dao.FlightDao;
import service.Flight;

import java.io.*;
import java.util.ArrayList;
import java.util.List;


public class FileFlightDao implements FlightDao {
    private List<Flight> flightList;
    private String filePath;

    public FileFlightDao() {
        flightList = new ArrayList<>();
    }
    @Override
    public List<Flight> getAllFlights() {
        return flightList;
    }

    @Override
    public Flight getFlightById(int id) {
        return flightList.stream()
                .filter(flight -> flight.getFlightId(id) == id).findFirst().orElse(null);
    }

    @Override
    public void addFlight(Flight flight) {
        flightList.add(flight);
        saveFlightToFile(flightList);
    }

    @Override
    public void updateFlight(Flight flight) {

    }

    @Override
    public void deleteFlight(int id) {

    }

//    public void saveFightToFile(Flight flight){
//        try (ObjectOutputStream oos= new ObjectOutputStream(new FileOutputStream("D:\\Projects\\Java\\DanIt\\Homework\\Homework\\StepProject\\flightDataBase.dat"))){
//            oos.writeObject(flight);
//        } catch (IOException e) {
//            System.out.println("Не вдалося сохранити файл" +e);
//            e.printStackTrace(); //TODO: Удалить после полной проверки
//            }
//    }
//
//    public void loadFlightToFile() {
//        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("D:\\Projects\\Java\\DanIt\\Homework\\Homework\\StepProject\\flightDataBase.dat"))){
//            List<Flight> deserilizationList = (List<Flight>) ois.readObject();
//            System.out.println(deserilizationList.toString());
//        } catch (IOException | ClassNotFoundException e) {
//            throw new RuntimeException(e);
//        }
//    }

    public void saveFlightToFile(List<Flight> flightList) {
        try (ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("D:\\Projects\\Java\\DanIt\\Homework\\Homework\\StepProject\\flightDataBase.dat"))) {
            oos.writeObject(flightList);
        } catch (IOException e) {
            System.out.println("Не вдалося зберегти файл: " + e);
        }
    }

    public List<Flight> loadFlightFromFile() {
        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("D:\\Projects\\Java\\DanIt\\Homework\\Homework\\StepProject\\flightDataBase.dat"))) {
            return (List<Flight>) ois.readObject();

        } catch (IOException | ClassNotFoundException e) {
            System.out.println("Не вдалося зберегти файл: " + e);
            throw new RuntimeException(e);
        }
    }
}
