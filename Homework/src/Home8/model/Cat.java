package Home8.model;

import Home6.model.Fouling;
import Home8.dao.Species;
import Home8.interfac.PetHabits;

import java.util.Set;
public class Cat extends Pet implements Fouling, PetHabits {
    private Set<String> habits;

    public Cat(String nickname) {
        super(Species.CAT.getName(), nickname);
    }

    public Cat(String nickname, Set<String> habits) {
        super(Species.CAT.getName(), nickname, habits);
    }

    public Cat(String species, String nickname, int age, int trickLevel, Set<String> habits) {
        super(species, nickname, age, trickLevel, habits);
        this.habits = habits;
    }

    @Override
    public Set<String> getHabits() {
        return habits;
    }

    @Override
    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    @Override
    protected void eat() {
        System.out.println("Кошка ест рыбу");
    }

    @Override
    protected void respond() {
        System.out.println("Мяу!");
    }

    @Override
    public void foul() {
        System.out.printf("(%s) Нужно хорошенько убрать следы...%n", getNickname());
    }
    @Override
    public String toString() {
        return "Cat{" +
                "species='" + getSpecies() + '\'' +
                ", nickname='" + getNickname() + '\'' +
                ", age=" + getAge() +
                ", trickLevel=" + getTrickLevel() +
                ", habits=" + habits +
                '}';
    }
}