package Home8.model;

import Home8.dao.Species;
import Home8.interfac.PetHabits;

import java.util.Set;

public class RoboCat extends Pet implements PetHabits {
    private Set<String> habits;
    @Override
    public Set<String> getHabits() {
        return habits;
    }

    @Override
    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }
    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits) {
        super(String.valueOf(Species.ROBOCAT), nickname, age, trickLevel, habits);
    }
    @Override
    public String toString() {
        return "Cat{" +
                "species='" + getSpecies() + '\'' +
                ", nickname='" + getNickname() + '\'' +
                ", age=" + getAge() +
                ", trickLevel=" + getTrickLevel() +
                ", habits=" + habits +
                '}';
    }
    @Override
    public void eat() {
        System.out.println("РобоКіт харчується спеціальним паливом.");
    }

    @Override
    public void respond() {
        System.out.println("Біп-боп! Я РобоКіт!");
    }


}
