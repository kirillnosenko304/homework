package Home8.model;

final class Woman extends Human {
    // Woman class implementation

    public void greetPet() {
        System.out.println("Привет, милый!");
    }

    public void makeup() {
        System.out.println("Пришло время сделать макияж.");
    }
}