package Home8.controll;

import Home8.interfac.FamilyDao;
import Home8.model.Family;
import Home8.model.Human;
import Home8.model.Pet;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class FamilyController extends Family implements FamilyDao {
    private List<Family> allFamilies;

    public FamilyController() {
        allFamilies = new ArrayList<>();
    }
    @Override
    public List<Family> getAllFamilies() {
        return allFamilies; // Так само выводит список семьи
    }

    @Override
    public void addFamily(Family family) {
        allFamilies.add(family);
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (allFamilies.contains(family)) {
            allFamilies.remove(family);
            return true;
        }
        return false;
    }
    //Метод сохранения семьи и проверка на нахождения семьи в списке
    @Override
    public boolean saveFamily(Family family) {
        if (allFamilies.contains(family)){
            System.out.println("Семья уже есть в списке");
            return true;
        } else{
            allFamilies.add(family);
            System.out.println("Семья успешно добавлена в список");
        }
        return false;
    }

    @Override
    public void displayAllFamilies() {
        for (Family family : allFamilies) {
            System.out.println(family.toString()); // Вивести інформацію про сім'ю на екран
        }
    }
    // Метод знаходить сім'ї з кількістю людей більше ніж зазначена кількість
    public List<Family> getFamiliesBiggerThan(int number) {
        List<Family> result = new ArrayList<>();
        for (Family family : allFamilies) {
            int familySize = family.countFamily();
            if (familySize > number) {
                result.add(family);
            }
        }
        return result;
    }

    // Метод знаходить сім'ї з кількістю людей менше ніж зазначена кількість
    public List<Family> getFamiliesLessThan(int number) {
        List<Family> result = new ArrayList<>();
        for (Family family : allFamilies) {
            int familySize = family.countFamily();
            if (familySize < number) {
                result.add(family);
            }
        }
        return result;
    }

    // Метод підраховує кількість сімей з заданою кількістю членів
    public int countFamiliesWithMemberNumber(int number) {
        int count = 0;
        for (Family family : allFamilies) {
            int familySize = family.countFamily();
            if (familySize == number) {
                count++;
            }
        }
        return count;
    }

    // Метод створює нову сім'ю і додає її до списку allFamilies
    public void createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        family.setMother(mother);
        family.setFather(father);
        allFamilies.add(family);
    }

    // Метод видаляє сім'ю за індексом у списку
    public boolean deleteFamilyByIndex(int index) {
        if (index >= 0 && index < allFamilies.size()) {
            allFamilies.remove(index);
            return true;
        }
        return false;
    }

    // Метод народжує сім'єю дитину з заданим іменем (з урахуванням статі батьків)
    public Human bornChild(Family family, String childName) {
        Human child;
        if (family.getMother() != null && family.getFather() != null) {
            int motherIq = family.getMother().getIq();;
            int fatherIq = family.getFather().getIq();

            // Перевірка на null і встановлення дефолтного значення, якщо один із батьків не має IQ
            int childIq = (motherIq + fatherIq) / 2;

            String childGender = getRandomGender();
            child = new Human(childName, family.getFather().getSurname(), childGender, 0 , childIq);
            family.addChild(child);
            return child;
        }
        return null;
    }


    // Метод усиновлює дитину і додає її до сім'ї
    public void adoptChild(Family family, Human child) {
        family.addChild(child);
    }

    // Метод видаляє всіх дітей старше заданого віку
    public void deleteAllChildrenOlderThen(int age) {
        for (Family family : allFamilies) {
            List<Human> children = family.getChildren();
            children.removeIf(child -> child.getYear() > age);
        }
    }

    // Метод повертає кількість сімей у списку allFamilies
    public int count() {
        return allFamilies.size();
    }

    // Метод повертає Family за вказаним індексом
    @Override
    public Family getFamilyByIndex(int index) {
        if (index >= 0 && index < allFamilies.size()) {
            return allFamilies.get(index);
        }
        return null;
    }
    // Метод повертає список свійських тварин, які живуть у сім'ї
    public List<Pet> getPets(int familyIndex) {
        if (familyIndex >= 0 && familyIndex < allFamilies.size()) {
            Family family = allFamilies.get(familyIndex);
            return new ArrayList<>(family.getPets());
        }
        return null;
    }

    // Метод додає нового вихованця в сім'ю
    public void addPet(int familyIndex, Pet pet) {
        if (familyIndex >= 0 && familyIndex < allFamilies.size()) {
            Family family = allFamilies.get(familyIndex);
            family.getPets().add(pet);
        }
    }

    // Метод для випадкового вибору статі дитини (метод вам не потрібно додавати у ваш код)
    private String getRandomGender() {
        Random random = new Random();
        return random.nextBoolean() ? "male" : "female";
    }
}
