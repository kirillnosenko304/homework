package Home8.controll;

import Home8.interfac.FamilyDao;
import Home8.model.Family;

import java.util.List;

public class FamilyService {
    private final FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }
}
