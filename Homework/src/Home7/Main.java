package Home7;

import Home7.interfac.PetHabits;
import Home7.model.*;

import java.util.*;

public class Main {
    public static void main(String[] args) {
          //Correct
        Set<String> catHabits = new HashSet<>();
        catHabits.add("прыгать на стол");
        catHabits.add("играть с мячиком");

        //correct
        /*
        Cat cat = new Cat("Мурка");
        cat.setHabits(catHabits);
        System.out.println(cat.toString());
        System.out.println("\n");*/

          //Correct
        /*
        RoboCat roboCat = new RoboCat("RoboKitty", 2, 5, catHabits);
        roboCat.printPet();
        System.out.println(roboCat.toString());
        roboCat.eat();
        roboCat.respond(); */


        // Створення об'єкту сім'ї
        Couple couple = new Couple();
        Human child = couple.bornChild();

        Set<String> habits = null;
        Family family = new Family("Мати", "Батько", Collections.singleton(new Pet("Собака", "Steve") {
            @Override
            public Set<String> getHabits() {
                return null;
            }

            @Override
            public void setHabits(Set<String> habits) { //TODO: Не могу попять причину по которой я не могу создать привычку через интерфейс, не могу понять почему
                                                        //11-13 строка. Таким способом получается
                if (habits != null) {
                    habits.add("БЕГАТЬ ЯК ФЛЕШ");
                    System.out.println(habits);
                }

            }

            @Override
            protected void eat() {

            }

            @Override
            protected void respond() {

            }
        }));
        habits = new HashSet<>();
        habits.add("прыгать на стол");
        habits.add("играть с мячиком");


        System.out.println(family.toString());
        // Створення дитини
        Family family1 = new Family("Мать", "Отец"); // Создаем объект Family
        Human child1 = new Human("Имя ребенка", "Фамилия ребенка", 2022, 100); // Создаем объект Human для ребенка
        family.addChild(child, "Имя ребенка","Фамилия", 5 , 44 ); // Добавляем ребенка в семью
        // Проверяем, что ребенок добавлен успешно
        List<Human> children = family.getChildren();
        System.out.println(children); // Выведет список детей в семье

        //Correct
        /*Human human = new Human();
        Map<String, String> schedule = new HashMap<>();
        schedule.put("Monday", "Work");
        schedule.put("Tuesday", "Gym");
        schedule.put("Wednesday", "Meeting");
        human.setSchedule(schedule);

        // Отримати розклад
        Map<String, String> humanSchedule = human.getSchedule();
        System.out.println(humanSchedule.get("Monday")); // Виведе "Work"
        System.out.println(humanSchedule.get("Wednesday")); // Виведе "Meeting"

        // Створення списку дітей
        Family family = new Family("Мати", "Батько");
        family.addChild(new Human(), "Прізвище2", "4", 52, 3);
        family.addChild(new Human(), "name", "surname", 52, 3);


        // Отримання списку дітей з сім'ї
        List<Human> childrenList = family.getChildren();

        // Виведення імен дітей
        for (Human child : childrenList) {
            System.out.println(child);
        }
        */
    }
}
