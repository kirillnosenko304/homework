package Home7.model;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Family extends Human {
    private String mother;
    private String father;
    private List<Human> children = new ArrayList<>();
    private Set<Pet> pets = new HashSet<>();
    private Family family;
    private int originalChildrenLength;

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }
    public Family getFamily() {
        return family;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }
    public Set<Pet> getPets() {
        return pets;
    }
    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public String toString() {
        return "Family{\n" +
                "  mother='" + mother + "',\n" +
                "  father='" + father + "',\n" +
                "  children=" + children + ",\n" +
                "  pet=" + pets + "\n" +
                "}";
    }

    public void addChild(Human child,String nameChild, String surname, int year, int iq) {
        child.setName(nameChild);
        child.setSurname(surname);
        child.setFamily(this);
        children.add(child);
        setYear(year);
        setIq(iq);

        // Перевірка, що список children збільшується
        if (children.size() == originalChildrenLength + 1 && children.get(children.size() - 1) == child) {
            System.out.println("Список children збільшується на один елемент, яким є переданий об'єкт");
        }
    }
    public void deleteChild(Family child) {
        boolean childRemoved = children.remove(child);
        if (!childRemoved) {
            System.out.println("Дитина не знайдена у списку children");
        }

        // Перевірка, що список children залишається без змін
        boolean childrenUnchanged = isChildrenUnchanged(child);
        if (!childrenUnchanged) {
            System.out.println("Список children змінився");
        }

        // Перевірка, що дитина вже не належить сім'ї
        boolean childNoLongerInFamily = child.getFamily() != this;
        if (!childNoLongerInFamily) {
            System.out.println("Дитина більше не належить цій сім'ї");
        }
    }

    private boolean isChildrenUnchanged(Human excludedChild) {
        List<Human> expectedChildren = new ArrayList<>(children);
        expectedChildren.remove(excludedChild);
        return expectedChildren.equals(children);
    }

    public int countFamily() {
        return 2 + children.size(); // 2 для матери и отца
    }

    public Family(String mother, String father, List<Human> children) {
        if (mother != null && !mother.isEmpty() && father != null && !father.isEmpty()) {
            this.mother = mother;
            this.father = father;
            setChildren(children);
            this.children = new ArrayList<>();
        } else {
            System.out.println("Не удалось создать семью. Необходимо указать обоих родителей.");
        }
    }
    public Family(String mother, String father, Set<Pet> pets) {
        if (mother != null && !mother.isEmpty() && father != null && !father.isEmpty()) {
            this.mother = mother;
            this.father = father;
            setPets(pets);
            this.children = new ArrayList<>();
        } else {
            System.out.println("Не удалось создать семью. Необходимо указать обоих родителей.");
        }
    }
    public Family(String mother, String father) {
        if (mother != null && !mother.isEmpty() && father != null && !father.isEmpty()) {
            this.mother = mother;
            this.father = father;
            this.children = new ArrayList<>();
        } else {
            System.out.println("Не удалось создать семью. Необходимо указать обоих родителей.");
        }
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Удаление объекта Pet: " + family.getFamily());
        super.finalize();
    }
}
