package Home9.controll;

import Home9.interfac.FamilyDao;
import Home9.model.Family;

import java.util.List;

public class FamilyService {
    private final FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }
}
