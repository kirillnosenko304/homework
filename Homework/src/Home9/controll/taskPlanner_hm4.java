package Home9.controll;

import java.util.Scanner;

public class taskPlanner_hm4 {
        taskPlanner_hm4(){
            // Matrix
            String[][] tasksMatrix = new String[7][2];
            tasksMatrix[0] = new String[]{"Monday", "The main task for this day 1"};
            tasksMatrix[1] = new String[]{"Tuesday", "The main task for this day 2"};
            tasksMatrix[2] = new String[]{"Wednesday", "The main task for this day 3"};
            tasksMatrix[3] = new String[]{"Thursday", "The main task for this day 4"};
            tasksMatrix[4] = new String[]{"Friday", "The main task for this day 5"};
            tasksMatrix[5] = new String[]{"Saturday", "The main task for this day 6"};
            tasksMatrix[6] = new String[]{"Sunday", "The main task for this day 7"};

            boolean isValid = false;
            while (!isValid){
                Scanner scanner = new Scanner(System.in);
                System.out.print("Введіть день ");
                String numDay = scanner.nextLine().trim().toLowerCase();
                if (numDay.equalsIgnoreCase("exit") || numDay.equalsIgnoreCase("Вихід") ) {
                    isValid = true;
                    System.out.println("Дякую за використання програми. До побачення!");
                }else if ( (numDay.equalsIgnoreCase("help") || numDay.equalsIgnoreCase("-h")) ){
                    System.out.println("change(-c) --> Замінна таска\n" +
                            "show(-s)    --> Показ всього тиждня\n" +
                            "exit        --> Вихід з програми");
                } else if (numDay.equalsIgnoreCase("change") || numDay.equalsIgnoreCase("-c")) {
                    System.out.println("Що ви хочете замінити?");
                    updateTask(tasksMatrix, scanner.nextLine());
                } else if (numDay.equalsIgnoreCase("show") || numDay.equalsIgnoreCase("-s"))  {
                    showMatrix(tasksMatrix);
                } else {
                    checkShedule(numDay, tasksMatrix);
                }
            }
        }
        private static void showMatrix(String[][] tasksMatrix){
            for (String[] row : tasksMatrix) {
                System.out.println(row[0] + ": " +row[1]);
            }
        }
        private static void updateTask(String[][] tasksMatrix, String dayOfWeek) {
            Scanner scanner = new Scanner(System.in);
            for (String[] row : tasksMatrix) {
                if (row[0].equalsIgnoreCase(dayOfWeek)) {
                    System.out.print("Введіть нове завдання для " + dayOfWeek + ": ");
                    String newTask = scanner.nextLine().trim().toLowerCase();
                    row[1] = newTask;
                    System.out.println("Завдання для " + dayOfWeek + " оновлено.");
                    return;
                }
            }
            System.out.println("День недели не знайдено.");
        }
        private static void checkShedule(String number, String[][] taskMatrix){
            String day;
            String task;
            //int num = Integer.parseInt(number); del
            switch (number){
                default:
                    day = "";
                    task = "";
                    break;
                case "monday":
                    day = taskMatrix[0][0];
                    task = taskMatrix[0][1];
                    break;
                case "tuesday":
                    day = taskMatrix[1][0];
                    task = taskMatrix[1][1];
                    break;
                case "wednesday" :
                    day = taskMatrix[2][0];
                    task = taskMatrix[2][1];
                    break;
                case "thursday":
                    day = taskMatrix[3][0];
                    task = taskMatrix[3][1];
                    break;
                case "friday":
                    day = taskMatrix[4][0];
                    task = taskMatrix[4][1];
                    break;
                case "saturday":
                    day = taskMatrix[5][0];
                    task = taskMatrix[5][1];
                    break;
                case "sunday":
                    day = taskMatrix[6][0];
                    task = taskMatrix[6][1];
                    break;
            }
            if (day == "" || task == ""){ System.out.print("Вибачте, я вас не розумію, спробуйте ще раз\n");}
            else {
                System.out.println(day + ": " + task);
            }
        }

    }


