package Home9.model;

import java.util.*;

public class Family {
    private Human mother;
    private Human father;
    private List<Human> children = new ArrayList<>();
    private Set<Pet> pets = new HashSet<>();
    private static List<Family> allFamilies;
    private int originalChildrenLength;
    public Human getMother() {
        return this.mother;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return this.father;
    }

    public void setFather(Human father) {
        this.father = father;
    }
    public Family() {
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }
    public List<Family> getFamily() {
        return allFamilies;
    }

    public void setPets(Set<Pet> pets) {
        this.pets = pets;
    }
    public Set<Pet> getPets() {
        return pets;
    }
    public void setFamily(Family family) {
        this.allFamilies = Collections.singletonList(family);
    }
    @Override
    public String toString() {
        return "Family{\n" +
                "  mother='" + mother + "',\n" +
                "  father='" + father + "',\n" +
                "  children=" + children + ",\n" +
                "  pet=" + pets + "\n" +
                "}";
    }

    public void addChild(Human child) {
        child.setFamily(this);
        children.add(child);

        // Перевірка, що список children збільшується
        if (children.size() == originalChildrenLength + 1 && children.get(children.size() - 1) == child) {
            System.out.println("Список children збільшується на один елемент, яким є переданий об'єкт");
        }

    }
    public void deleteChild(Human child) {
        boolean childRemoved = children.remove(child);
        if (!childRemoved) {
            System.out.println("Дитина не знайдена у списку children");
        }

        // Перевірка, що список children залишається без змін
        boolean childrenUnchanged = isChildrenUnchanged(child);
        if (!childrenUnchanged) {
            System.out.println("Список children змінився");
        }

        // Перевірка, що дитина вже не належить сім'ї
        boolean childNoLongerInFamily = child.getFamily() != this;
        if (!childNoLongerInFamily) {
            System.out.println("Дитина більше не належить цій сім'ї");
        }
    }

    private boolean isChildrenUnchanged(Human excludedChild) {
        List<Human> expectedChildren = new ArrayList<>(children);
        expectedChildren.remove(excludedChild);
        return expectedChildren.equals(children);
    }

    public int countFamily() {
        return 2 + children.size(); // 2 для матери и отца
    }

    public Family(Human mother, Human father, List<Human> children) {
        if (mother != null && !mother.getFamily().isEmpty() && father != null && !father.getFamily().isEmpty()) {
            this.mother = mother;
            this.father = father;
            setChildren(children);
            this.children = new ArrayList<>();
        } else {
            System.out.println("Не удалось создать семью. Необходимо указать обоих родителей.");
        }

    }
    public Family(Human mother, Human father, Set<Pet> pets) {
        if (mother != null && !mother.getFamily().isEmpty() && father != null && !father.getFamily().isEmpty()) {
            this.mother = mother;
            this.father = father;
            setPets(pets);
            this.children = new ArrayList<>();
        } else {
            System.out.println("Не удалось создать семью. Необходимо указать обоих родителей.");
        }
    }

    public Family(Human mother, Human father) {
        if (mother != null && !mother.getFamily().isEmpty() && father != null && !father.getFamily().isEmpty()) {
            this.mother = mother;
            this.father = father;
            this.children = new ArrayList<>();
        } else {
            System.out.println("Не удалось создать семью. Необходимо указать обоих родителей.");
        }
    }

}
