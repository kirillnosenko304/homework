package Home9.model;

import Home9.interfac.PetHabits;

import java.util.Set;

public abstract class Pet implements PetHabits {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;

    public Pet(String species, String nickname, int age, int trickLevel, Set<String> habits) {
        setSpecies(species);
        setNickname(nickname);
        setAge(age);
        setTrickLevel(trickLevel);
        setHabits(habits);
    }

    public Pet(String species, String nickname) {
        setSpecies(species);setNickname(nickname);
    }

    public Pet(String species, String nickname, Set<String> habits) {
        this.species = species; this.nickname = nickname; this.habits=habits;


    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    @Override
    public Set<String> getHabits() {
        return habits;
    }

    @Override
    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    public String getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "species='" + species + '\'' +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + habits +
                '}';
    }

    public void printPet() {
        System.out.printf("\nПривет, хозяин, опять забыл про меня? Это я - %s. Я - %s, мне - %d, я хитрый зверь - %d%n\n", nickname, species, age, trickLevel);
    }

    protected abstract void eat();

    protected abstract void respond();

    protected void foul() {
        System.out.printf("(%S) Нужно хорошенько убрать следы...%n", this.nickname);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Deleting Pet object: " + nickname);
        super.finalize();
    }
}
