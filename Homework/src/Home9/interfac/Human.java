package Home9.interfac;

import java.util.Map;

public interface Human  {
    Map<String, String> getSchedule();
    void setSchedule(Map<String, String> schedule);
}
