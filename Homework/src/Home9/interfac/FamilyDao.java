package Home9.interfac;

import Home9.model.Family;
import Home9.model.Human;
import Home9.model.Pet;

import java.util.List;

public interface FamilyDao {
    List<Family> getAllFamilies();
    void addFamily(Family family);
    Family getFamilyByIndex(int index);
    boolean deleteFamily(Family family);
    boolean saveFamily(Family family);
    void displayAllFamilies();
    public Human bornChild(Family family, String childName);
    public void adoptChild(Family family1, Human child1);
    public int count();
    public void deleteAllChildrenOlderThen(int age);
    public List<Pet> getPets(int familyIndex);
    public void addPet(int familyIndex, Pet pet);
    public int countFamiliesWithMemberNumber(int number);
    public List<Family> getFamiliesLessThan(int number);
    public List<Family> getFamiliesBiggerThan(int number);
    public boolean deleteFamilyByIndex(int index);
    public void createNewFamily(Human mother, Human father);


    }
