package Home11.interfac;

import Home11.model.Family;
import Home11.model.Human;
import Home11.model.Pet;

import java.util.List;
import java.util.Optional;

public interface FamilyDao {
    List<Family> getAllFamilies();
    void addFamily(Family family);
    Optional<Family> getFamilyByIndex(int index);
    boolean deleteFamily(Family family);
    boolean saveFamily(Family family);
    void displayAllFamilies();
    public Human bornChild(Family family, String childName);
    public void adoptChild(Family family1, Human child1);
    public int count();
    public void deleteAllChildrenOlderThen(int age);
    public Optional<List<Pet>> getPets(int familyIndex);
    public void addPet(int familyIndex, Pet pet);
    public long countFamiliesWithMemberNumber(int number);
    public List<Family> getFamiliesLessThan(int number);
    public List<Family> getFamiliesBiggerThan(int number);
    public boolean deleteFamilyByIndex(int index);
    public void createNewFamily(Human mother, Human father);


    }
