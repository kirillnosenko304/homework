package Home11.dao;

public enum Species {
    UNKNOWN("Невідомий"),
    CAT("Кошка"),
    DOG("Собака"),
    LION("Лев"),
    TIGER("Тигр"),
    RABBIT("Заяц"),
    WOLF("Волк"),
    FOX("Лиса"),
    DEER("Олень"),
    GIRAFFE("Жираф"),
    ELEPHANT("Слон"),
    CUSTOM("Інший вид"),
    ROBOCAT("РобоКіт");


    private final String name;

    Species(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public static Species getSpeciesByValue(String value) {
        for (Species species : Species.values()) {
            if (species.name.equalsIgnoreCase(value)) {
                return species;
            }
        }
        return UNKNOWN;
    }
}
