package Home11.controll;

import Home11.interfac.FamilyDao;
import Home11.model.Family;

import java.util.List;

public class FamilyController {
    private final FamilyDao familyDao;

    public FamilyController(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }


}
