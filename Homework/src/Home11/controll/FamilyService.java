package Home11.controll;

import Home11.Exception.FamilyOverflowException;
import Home11.dao.Species;
import Home11.interfac.FamilyDao;
import Home11.model.Family;
import Home11.model.Human;
import Home11.model.Pet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class FamilyService implements FamilyDao {
    private static final int MAX_FAMILY_SIZE = 5;
    private List<Family> allFamilies;

    public FamilyService() {
        allFamilies = new ArrayList<>();
    }
    @Override
    public List<Family> getAllFamilies() {
        return allFamilies;
    }

    @Override
    public void addFamily(Family family) {
        allFamilies.add(family);
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (allFamilies.contains(family)) {
            allFamilies.remove(family);
            return true;
        }
        return false;
    }
    //Метод сохранения семьи и проверка на нахождения семьи в списке
    @Override
    public boolean saveFamily(Family family) {
        if (allFamilies.contains(family)){
            System.out.println("Семья уже есть в списке");
            return true;
        } else{
            allFamilies.add(family);
            System.out.println("Семья успешно добавлена в список");
        }
        return false;
    }

    @Override
    public void displayAllFamilies() {
        allFamilies.forEach(family -> System.out.println(family.toString()));
    }

    // Метод знаходить сім'ї з кількістю людей більше ніж зазначена кількість
    public List<Family> getFamiliesBiggerThan(int number) {
        return allFamilies.stream()
                .filter(family -> family.countFamily() > number)
                .collect(Collectors.toList());

    }

    @Override
    public boolean deleteFamilyByIndex(int index) {
        return false;
    }

    @Override
    public void createNewFamily(Human mother, Human father) {

    }

    public void displayFamiliesBiggerThan() {
        System.out.println("Введіть кількість людей, більшу за яку потрібно відобразити сім'ї:");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        scanner.nextLine(); // Очистити буфер після введення числа

        List<Family> filteredFamilies = getFamiliesBiggerThan(number);
        if (!filteredFamilies.isEmpty()) {
            System.out.println("Сім'ї з кількістю людей більше " + number + ":");
            filteredFamilies.forEach(family -> System.out.println(family.prettyFormat()));
        } else {
            System.out.println("Сім'ї з кількістю людей більше " + number + " не знайдено.");
        }
    }


    // Метод знаходить сім'ї з кількістю людей менше ніж зазначена кількість
    public List<Family> getFamiliesLessThan(int number) {
        return allFamilies.stream()
                .filter(family -> family.countFamily() < number)
                .collect(Collectors.toList());
    }

    public void displayFamiliesLessThan() {
        System.out.println("Введіть кількість людей, меншу за яку потрібно відобразити сім'ї:");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        scanner.nextLine(); // Очистити буфер після введення числа

        List<Family> filteredFamilies = getFamiliesLessThan(number);
        if (!filteredFamilies.isEmpty()) {
            System.out.println("Сім'ї з кількістю людей менше " + number + ":");
            filteredFamilies.forEach(family -> System.out.println(family.prettyFormat()));
        } else {
            System.out.println("Сім'ї з кількістю людей менше " + number + " не знайдено.");
        }
    }


    // Метод підраховує кількість сімей з заданою кількістю членів
    public long countFamiliesWithMemberNumber(int number) {
        return allFamilies.stream()
                .filter(family -> family.countFamily() == number)
                .count();
    }

    public void countFamiliesWithMemberNumber() {
        System.out.println("Введіть кількість членів сім'ї, для якої потрібно підрахувати кількість сімей:");
        Scanner scanner = new Scanner(System.in);
        int number = scanner.nextInt();
        scanner.nextLine(); // Очистити буфер після введення числа

        long count = countFamiliesWithMemberNumber(number);
        System.out.println("Кількість сімей з " + number + " членами: " + count);
    }



    // Метод створює нову сім'ю і додає її до списку allFamilies
// Метод створює нову сім'ю і додає її до списку allFamilies
    public void createNewFamily() {
        System.out.println("Введіть ім'я матері:");
        Scanner scanner = new Scanner(System.in);
        String motherName = scanner.nextLine();

        System.out.println("Введіть прізвище матері:");
        String motherSurname = scanner.nextLine();

        System.out.println("Введіть дату народження матері (формат: дд/мм/рррр):");
        String motherBirthDateStr = scanner.nextLine();

        System.out.println("Введіть IQ матері:");
        int motherIq = scanner.nextInt();
        scanner.nextLine();

        if (isValidDateFormat(motherBirthDateStr)) {
            Human mother = new Human(motherName, motherSurname, "Woman", motherBirthDateStr, motherIq);

            System.out.println("Введіть ім'я батька:");
            String fatherName = scanner.nextLine();

            System.out.println("Введіть прізвище батька:");
            String fatherSurname = scanner.nextLine();

            System.out.println("Введіть дату народження батька (формат: дд/мм/рррр):");
            String fatherBirthDateStr = scanner.nextLine();

            System.out.println("Введіть IQ батька:");
            int fatherIq = scanner.nextInt();
            scanner.nextLine();

            if (isValidDateFormat(fatherBirthDateStr)) {
                Human father = new Human(fatherName, fatherSurname, "Man", fatherBirthDateStr, fatherIq);
                Family family = new Family(mother, father);

                mother.setFamily(family);
                father.setFamily(family);

                allFamilies.add(family);
                System.out.println("Нова сім'я створена:");
                System.out.println(family.prettyFormat());
            } else {
                System.out.println("Некоректний формат дати для батька. Введіть дату у форматі: дд/мм/рррр");
                return;
            }
        } else {
            System.out.println("Некоректний формат дати для матері. Введіть дату у форматі: дд/мм/рррр");
            return;
        }
    }


    private static boolean isValidDateFormat(String dateStr) {
        String regex = "\\d{2}/\\d{2}/\\d{4}";
        return dateStr.matches(regex);
    }

    // Метод видаляє сім'ю за індексом у списку
    public void deleteFamilyByIndex() {
        System.out.println("Введіть індекс сім'ї, яку потрібно видалити:");
        Scanner scanner = new Scanner(System.in);
        int index = scanner.nextInt();
        scanner.nextLine();

        boolean deleted = deleteFamilyByIndex(index);
        if (deleted) {
            System.out.println("Сім'ю з індексом " + index + " успішно видалено.");
        } else {
            System.out.println("Сім'ю з індексом " + index + " не знайдено.");
        }
    }

    // Метод для редагування сім'ї
    public void editFamilyByIndex() {
        System.out.println("Введіть індекс сім'ї, яку ви хочете відредагувати:");
        Scanner scanner = new Scanner(System.in);
        int familyIndex = scanner.nextInt();
        scanner.nextLine(); // Очистити буфер після введення числа

        if (familyIndex >= 0 && familyIndex < allFamilies.size()) {
            Family family = allFamilies.get(familyIndex);

            System.out.println("Виберіть, що ви хочете змінити:");
            System.out.println("1. Ім'я матері");
            System.out.println("2. Прізвище матері");
            System.out.println("3. Дату народження матері");
            System.out.println("4. IQ матері");
            System.out.println("5. Ім'я батька");
            System.out.println("6. Прізвище батька");
            System.out.println("7. Дату народження батька");
            System.out.println("8. IQ батька");
            System.out.println("9. Вихід");

            int option = scanner.nextInt();
            scanner.nextLine(); // Очистити буфер після введення числа

            switch (option) {
                case 1 -> {
                    System.out.println("Введіть нове ім'я матері:");
                    String motherName = scanner.nextLine();
                    family.getMother().setName(motherName);
                }
                case 2 -> {
                    System.out.println("Введіть нове прізвище матері:");
                    String motherSurname = scanner.nextLine();
                    family.getMother().setSurname(motherSurname);
                }
                case 3 -> {
                    System.out.println("Введіть нову дату народження матері (формат: дд/мм/рррр):");
                    String motherBirthDateStr = scanner.nextLine();
                    if (isValidDateFormat(motherBirthDateStr)) {
                        family.getMother().setBirthDate(Long.parseLong(motherBirthDateStr));
                    } else {
                        System.out.println("Некоректний формат дати для матері. Введіть дату у форматі: дд/мм/рррр");
                        return;
                    }
                }
                case 4 -> {
                    System.out.println("Введіть новий IQ матері:");
                    int motherIQ = scanner.nextInt();
                    family.getMother().setIq(motherIQ);
                }
                case 5 -> {
                    System.out.println("Введіть нове ім'я батька:");
                    String fatherName = scanner.nextLine();
                    family.getFather().setName(fatherName);
                }
                case 6 -> {
                    System.out.println("Введіть нове прізвище батька:");
                    String fatherSurname = scanner.nextLine();
                    family.getFather().setSurname(fatherSurname);
                }
                case 7 -> {
                    System.out.println("Введіть нову дату народження батька (формат: дд/мм/рррр):");
                    String fatherBirthDateStr = scanner.nextLine();
                    if (isValidDateFormat(fatherBirthDateStr)) {
                        family.getFather().setBirthDate(Long.parseLong(fatherBirthDateStr));
                    } else {
                        System.out.println("Некоректний формат дати для матері. Введіть дату у форматі: дд/мм/рррр");
                        return;
                    }
                }
                case 8 -> {
                    System.out.println("Введіть новий IQ батька:");
                    int fatherIQ = scanner.nextInt();
                    family.getFather().setIq(fatherIQ);
                }
                case 9 -> {
                    System.out.println("Редагування сім'ї відмінено.");
                    return;
                }
                default -> {
                    System.out.println("Невідома опція. Введіть коректний номер опції.");
                    return;
                }
            }

            System.out.println("Сім'ю успішно відредаговано.");
        } else {
            System.out.println("Неправильний індекс сім'ї. Введіть коректний номер сім'ї для редагування.");
        }
    }


    // Метод народжує сім'єю дитину з заданим іменем (з урахуванням статі батьків)
    public Human bornChild(Family family, String childName) {
        Human child;

        if (family.countFamily() >= MAX_FAMILY_SIZE) {
            throw new FamilyOverflowException("Сім'я перевищує бажану кількість осіб і не може народити/усиновити дитину.");
        }
        if (family.getMother() != null && family.getFather() != null) {
            int motherIq = family.getMother().getIq();;
            int fatherIq = family.getFather().getIq();

            // Перевірка на null і встановлення дефолтного значення, якщо один із батьків не має IQ
            int childIq = (motherIq + fatherIq) / 2;

            // Получение текущей даты
            Date currentDate = new Date();

            // Форматирование даты в строку, если нужно
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String formattedDate = dateFormat.format(currentDate);

            String childGender = getRandomGender();
            child = new Human(childName, family.getFather().getSurname(), childGender, formattedDate, childIq);
            family.addChild(child);
            return child;
        }
        return null;
    }


    // Метод усиновлює дитину і додає її до сім'ї
    public void adoptChild(Family family, Human child) {
        family.addChild(child);
    }

    // Метод видаляє всіх дітей старше заданого віку
    public void deleteAllChildrenOlderThen() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введіть вік, старший за який ви хочете видалити дітей:");
        int age = scanner.nextInt();
        allFamilies.forEach(family -> family.getChildren().removeIf(child -> child.getBirthDate() > age));
        System.out.println("Діти старше " + age + " років видалені з усіх сімей.");
    }

    // Метод повертає кількість сімей у списку allFamilies
    public int count() {
        return allFamilies.size();
    }

    @Override
    public void deleteAllChildrenOlderThen(int age) {

    }

    // Метод повертає Family за вказаним індексом
    @Override
    public Optional<Family> getFamilyByIndex(int index) {
        return index >= 0 && index < allFamilies.size() ? Optional.of(allFamilies.get(index)) : Optional.empty();
    }
    // Метод повертає список свійських тварин, які живуть у сім'ї
    public Optional<List<Pet>> getPets(int familyIndex) {
        return (familyIndex >= 0 && familyIndex < allFamilies.size())
                ? Optional.of(new ArrayList<>(allFamilies.get(familyIndex).getPets()))
                : Optional.empty();

    }

    // Метод додає нового вихованця в сім'ю
    public void addPet(int familyIndex, Pet pet) {
        getFamilyByIndex(familyIndex).ifPresent(family -> family.getPets().add(pet));
    }

    // Метод для випадкового вибору статі дитини (метод вам не потрібно додавати у ваш код)
    private String getRandomGender() {
        Random random = new Random();
        return random.nextBoolean() ? "male" : "female";
    }

    //Тестові дані
    public void fillWithTestData() {
        // Создаем несколько объектов Human для тестовых данных
        Human mother1 = new Human("Anna", "Smith", "Woman", "01/01/1985", 110);
        Human father1 = new Human("John", "Smith", "Man", "15/05/1980", 115);
        Human mother2 = new Human("Maria", "Johnson", "Woman", "10/10/1990", 105);
        Human father2 = new Human("Michael", "Johnson", "Man", "25/12/1988", 120);

        // Создаем несколько объектов Pet для тестовых данных


        Set<String> dogHabits = new HashSet<>();
        dogHabits.add("прыгать на стол");
        dogHabits.add("играть с мячиком");

        Pet pet1 = new Pet(Species.DOG, "Buddy", 3, 75,dogHabits ) {
            @Override
            protected void eat() {

            }
            @Override
            protected void respond() {

            }
        };
        Pet pet2 = new Pet(Species.CAT, "Luna", 5, 60, Set.of("Кусаться")) {
            @Override
            protected void eat() {

            }

            @Override
            protected void respond() {

            }
        };
        Pet pet3 = new Pet(Species.TIGER, "Charlie", 2, 50, Set.of("Грызть мебель")) {
            @Override
            protected void eat() {

            }

            @Override
            protected void respond() {

            }
        };

        // Создаем несколько объектов Family для тестовых данных
        Family family1 = new Family(mother1, father1);
        family1.addPet(pet1);
        family1.addPet(pet2);

        Family family2 = new Family(mother2, father2);
        family2.addPet(pet3);

        // Добавляем созданные семьи в список allFamilies
        allFamilies.add(family1);
        allFamilies.add(family2);

        System.out.println("Тестові дані додані.");
    }


    // Метод для виведення списку доступних команд
    private void displayMenu() {
        System.out.println("Список доступних команд:");
        System.out.println("1. Заповнити тестовими даними");
        System.out.println("2. Відобразити весь список сімей");
        System.out.println("3. Відобразити список сімей, де кількість людей більша за задану");
        System.out.println("4. Відобразити список сімей, де кількість людей менша за задану");
        System.out.println("5. Підрахувати кількість сімей, де кількість членів дорівнює");
        System.out.println("6. Створити нову родину");
        System.out.println("7. Видалити сім'ю за індексом сім'ї у загальному списку");
        System.out.println("8. Редагувати сім'ю за індексом сім'ї у загальному списку");
        System.out.println("9. Видалити всіх дітей старше віку");
        System.out.println("0. Вихід");
        System.out.println("Введіть номер команди:");
    }

    public void run() {
        int command;
        do {
            displayMenu();
            Scanner scanner = new Scanner(System.in);
            command = scanner.nextInt();
            scanner.nextLine(); // Очистити буфер після введення числа

            switch (command) {
                case 1 ->
                    // Заповнити тестовими даними
                        fillWithTestData();
                case 2 ->
                    // Відобразити весь список сімей
                        displayAllFamilies();
                case 3 ->
                    // Відобразити список сімей, де кількість людей більша за задану
                        displayFamiliesBiggerThan();
                case 4 ->
                    // Відобразити список сімей, де кількість людей менша за задану
                        displayFamiliesLessThan();
                case 5 ->
                    // Підрахувати кількість сімей, де кількість членів дорівнює
                        countFamiliesWithMemberNumber();
                case 6 ->
                    // Створити нову родину
                        createNewFamily();
                case 7 ->
                    // Видалити сім'ю за індексом сім'ї у загальному списку
                        deleteFamilyByIndex();
                case 8 ->
                    // Редагувати сім'ю за індексом сім'ї у загальному списку
                        editFamilyByIndex();
                case 9 ->
                    // Видалити всіх дітей старше віку
                        deleteAllChildrenOlderThen();
                case 0 -> System.out.println("Дякуємо за використання додатка. До побачення!");
                default -> System.out.println("Невідома команда. Введіть коректний номер команди.");
            }
        } while (command != 0);
    }
}
