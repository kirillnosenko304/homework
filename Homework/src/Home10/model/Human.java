package Home10.model;

import Home10.taskPlanner_hm4;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Period;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.List;
import java.util.Map;

enum WorkSchedule {
    PROGRAMMER("Программист"),
    DOCTOR("Врач"),
    TEACHER("Учитель"),
    ENGINEER("Инженер"),
    ARTIST("Художник"),
    ATHLETE("Спортсмен"),
    MUSICIAN("Музыкант"),
    WRITER("Писатель"),
    CHEF("Повар"),
    SCIENTIST("Ученый");

    private final String occupation;

    WorkSchedule(String occupation) {
        this.occupation = occupation;
    }

    public String getOccupation() {
        return occupation;
    }
}

public class Human implements Home10.interfac.Human {
    private Map<String, String> schedule;
    private String name;
    private String surname;
    private String Gender;
    private long birthDate;;
    private int iq;
    private Family family;
    private Home10.taskPlanner_hm4 taskPlanner_hm4;

    public Human(String name, String surname, String gender, String birthDateStr, int iq) {
        this.name = name;
        this.surname = surname;
        setGender(gender);
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate birthDate = LocalDate.parse(birthDateStr, formatter);
        LocalDateTime startOfDay = birthDate.atStartOfDay();
        this.birthDate = startOfDay.toInstant(ZoneOffset.UTC).toEpochMilli();


        this.iq = iq;
    }
    public String toString() {
        LocalDate birthLocalDate = LocalDate.ofEpochDay(birthDate / (24 * 60 * 60 * 1000));
        return "Human{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", birthDate=" + birthLocalDate.format(DateTimeFormatter.ofPattern("dd/MM/yyyy")) +
                ", iq=" + iq +
                '}';
    }

    public String describeAge() {
        LocalDate currentDate = LocalDate.now();
        LocalDate birthLocalDate = LocalDate.ofEpochDay(birthDate / (24 * 60 * 60 * 1000));
        Period age = Period.between(birthLocalDate, currentDate);
        return String.format("Вік: %d років, %d місяців, %d днів", age.getYears(), age.getMonths(), age.getDays());
    }
    @Override
    public Map<String, String> getSchedule() {
        return schedule;
    }
    public void setBirthDate(long birthDate) {
        this.birthDate = birthDate;
    }

    @Override
    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }
    public String getName() {
        return name;
    }

    public long getBirthDate() {
        return birthDate;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }
    public void setYear(int year) {
        this.birthDate = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }
    public String getGender() {
        return Gender;
    }

    public void setGender(String gender) {
        Gender = gender;
    }
    public Human() {}

    public void greetPet() {
        System.out.println("Привет, приятель!");
    }
    protected void describePet(Pet pet) {
        System.out.printf("У меня есть %s, ей %d лет, она %s\n", pet.getSpecies(), pet.getAge(), threshold(pet));
    }

    protected void taskPlanner() {
        Home10.taskPlanner_hm4 taskPlan = new taskPlanner_hm4();
    }

    private String threshold(Pet pet) {
        if (pet.getTrickLevel() >= 50) {
            return "очень хитрая";
        } else {
            return "почти не хитрая";
        }
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Удаление объекта Human: " + name);
        super.finalize();
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public List<Family> getFamily() {
        return Collections.singletonList(family);
    }
}

