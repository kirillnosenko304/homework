package Home10.model;

final class Man extends Human {
    public void greetPet() {
        System.out.println("Привет, малыш!");
    }

    public void repairCar() {
        System.out.println("Нужно отправить машину на СТО.");
    }
}
