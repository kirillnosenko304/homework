package Home10.controll;

import Home10.interfac.FamilyDao;
import Home10.model.Family;
import Home10.model.Human;
import Home10.model.Pet;

import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class FamilyService extends Family implements FamilyDao {
    private List<Family> allFamilies;

    public FamilyService() {
        allFamilies = new ArrayList<>();
    }
    @Override
    public List<Family> getAllFamilies() {
        return allFamilies;
    }

    @Override
    public void addFamily(Family family) {
        allFamilies.add(family);
    }

    @Override
    public boolean deleteFamily(Family family) {
        if (allFamilies.contains(family)) {
            allFamilies.remove(family);
            return true;
        }
        return false;
    }
    //Метод сохранения семьи и проверка на нахождения семьи в списке
    @Override
    public boolean saveFamily(Family family) {
        if (allFamilies.contains(family)){
            System.out.println("Семья уже есть в списке");
            return true;
        } else{
            allFamilies.add(family);
            System.out.println("Семья успешно добавлена в список");
        }
        return false;
    }

    @Override
    public void displayAllFamilies() {
        allFamilies.forEach(family -> System.out.println(family.toString()));
    }

    // Метод знаходить сім'ї з кількістю людей більше ніж зазначена кількість
    public List<Family> getFamiliesBiggerThan(int number) {
        return allFamilies.stream()
                .filter(family -> family.countFamily() > number)
                .collect(Collectors.toList());

    }

    // Метод знаходить сім'ї з кількістю людей менше ніж зазначена кількість
    public List<Family> getFamiliesLessThan(int number) {
        return allFamilies.stream()
                .filter(family -> family.countFamily() < number)
                .collect(Collectors.toList());
    }

    // Метод підраховує кількість сімей з заданою кількістю членів
    public long countFamiliesWithMemberNumber(int number) {
        return allFamilies.stream()
                .filter(family -> family.countFamily() == number)
                .count();
    }

    // Метод створює нову сім'ю і додає її до списку allFamilies
    public void createNewFamily(Human mother, Human father) {
        Family family = new Family(mother, father);
        family.setMother(mother);
        family.setFather(father);
        allFamilies.add(family);
    }

    // Метод видаляє сім'ю за індексом у списку
    public boolean deleteFamilyByIndex(int index) {
        if (index >= 0 && index < allFamilies.size()) {
            allFamilies.remove(index);
            return true;
        }
        return false;
    }

    // Метод народжує сім'єю дитину з заданим іменем (з урахуванням статі батьків)
    public Human bornChild(Family family, String childName) {
        Human child;
        if (family.getMother() != null && family.getFather() != null) {
            int motherIq = family.getMother().getIq();;
            int fatherIq = family.getFather().getIq();

            // Перевірка на null і встановлення дефолтного значення, якщо один із батьків не має IQ
            int childIq = (motherIq + fatherIq) / 2;

            // Получение текущей даты
            Date currentDate = new Date();

            // Форматирование даты в строку, если нужно
            SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
            String formattedDate = dateFormat.format(currentDate);

            String childGender = getRandomGender();
            child = new Human(childName, family.getFather().getSurname(), childGender, formattedDate, childIq);
            family.addChild(child);
            return child;
        }
        return null;
    }


    // Метод усиновлює дитину і додає її до сім'ї
    public void adoptChild(Family family, Human child) {
        family.addChild(child);
    }

    // Метод видаляє всіх дітей старше заданого віку
    public void deleteAllChildrenOlderThen(int age) {
        allFamilies.forEach(family -> family.getChildren().removeIf(child -> child.getBirthDate() > age));
    }

    // Метод повертає кількість сімей у списку allFamilies
    public int count() {
        return allFamilies.size();
    }

    // Метод повертає Family за вказаним індексом
    @Override
    public Optional<Family> getFamilyByIndex(int index) {
        return index >= 0 && index < allFamilies.size() ? Optional.of(allFamilies.get(index)) : Optional.empty();
    }
    // Метод повертає список свійських тварин, які живуть у сім'ї
    public Optional<List<Pet>> getPets(int familyIndex) {
        return (familyIndex >= 0 && familyIndex < allFamilies.size())
                ? Optional.of(new ArrayList<>(allFamilies.get(familyIndex).getPets()))
                : Optional.empty();

    }

    // Метод додає нового вихованця в сім'ю
    public void addPet(int familyIndex, Pet pet) {
        getFamilyByIndex(familyIndex).ifPresent(family -> family.getPets().add(pet));
    }

    // Метод для випадкового вибору статі дитини (метод вам не потрібно додавати у ваш код)
    private String getRandomGender() {
        Random random = new Random();
        return random.nextBoolean() ? "male" : "female";
    }
}
