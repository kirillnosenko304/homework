package Home10.controll;

import Home10.interfac.FamilyDao;
import Home10.model.Family;

import java.util.List;

public class FamilyController {
    private final FamilyDao familyDao;

    public FamilyController(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }
}
