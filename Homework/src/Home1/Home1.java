package Home1;

import java.util.ArrayList;
import java.util.Random;
import java.util.Scanner;

public class Home1 {
    public static void main(String[] args) {

        Random random = new Random();
        int targetNumber = random.nextInt(100);
        Scanner scanner = new Scanner(System.in);

        System.out.println("Let the game begin!");
        System.out.print("Enter your name: ");
        String name = scanner.nextLine();

        ArrayList<Integer> guesses = new ArrayList();
        int attempt = 0;

        while (true) {
            System.out.print("Enter a number: ");
            int guess = scanner.nextInt();
            guesses.add(guess);

            if (guess < targetNumber) {
                System.out.println("Your number is too small. Please, try again.");
            } else if (guess > targetNumber) {
                System.out.println("Your number is too big. Please, try again.");
            } else {
                System.out.printf("Congratulations, %s!%n", name);
                break;
            }
            attempt++;
        }
        guesses.sort(null);
        System.out.print("Ur numbers: "+ guesses);
    }
}
