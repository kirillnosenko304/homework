//package Home5;
//
//import java.util.Arrays;
//enum WorkSchedule {
//    PROGRAMMER("Программист"),
//    DOCTOR("Врач"),
//    TEACHER("Учитель"),
//    ENGINEER("Инженер"),
//    ARTIST("Художник"),
//    ATHLETE("Спортсмен"),
//    MUSICIAN("Музыкант"),
//    WRITER("Писатель"),
//    CHEF("Повар"),
//    SCIENTIST("Ученый");
//    private final String occupation;
//    WorkSchedule(String occupation) {
//        this.occupation = occupation;
//    }
//    public String getOccupation() {
//        return occupation;
//    }
//}
//public class Human implements AutoCloseable{
//    private Family family;
//    taskPlanner_hm4 taskPlanner_hm4;
//    public Human(String name, String surname, int year, int iq) {
//        this.name = name;
//        this.surname = surname;
//        this.year = year;
//        this.iq = iq;
//    }
//    public String toString(Pet pet) {
//        return "Human{" +
//                "name='" + name + '\'' +
//                ", surname='" + surname + '\'' +
//                ", year=" + year +
//                ", iq=" + iq +
//                '}' + "Pet{" +
//                "species='" + pet.getSpecies() + '\'' +
//                ", nickname='" + pet.getNickname() + '\'' +
//                ", age=" + pet.getAge() +
//                ", trickLevel=" + pet.getTrickLevel() +
//                ", habits=" + Arrays.toString(pet.getHabits()) +
//                '}';
//    }
//    public void setFamily(Family family) {
//        this.family = family;
//    }
//    public String getName() {
//        return name;
//    }
//    public void setName(String name) {
//        this.name = name;
//    }
//    public String getSurname() {
//        return surname;
//    }
//    public void setSurname(String surname) {
//        this.surname = surname;
//    }
//    public int getYear() {
//        return year;
//    }
//    public void setYear(int year) {
//        this.year = year;
//    }
//    public int getIq() {
//        return iq;
//    }
//    public void setIq(int iq) {
//        this.iq = iq;
//    }
//    private String name, surname;
//    private int year, iq;
//    Human(){
//        System.out.println("Конструктор Human");
//    }
//    protected void greetPet(Pet pet){
//        System.out.println("Хаюшки " + pet.getNickname());
//    }
//    protected void descrivePet(Pet pet){
//        System.out.printf("У мене є %s, їй %d років, він %s\n", pet.getSpecies(), pet.getAge(), threshold(pet)); // TODO: если будет лишний пробел
//    }
//    protected void taskPlanner() {
//        taskPlanner_hm4 taskPlan = new taskPlanner_hm4();
//    }
//    private String threshold(Pet pet){
//        if (pet.getTrickLevel() >= 50){
//            return "сука дуже хитрий";
//        } else {
//            return "майже не хитрий";
//        }
//    }
//    @Override
//    public void close() throws Exception {  // "Начиная с Java 9, метод finalize() в классе Object является устаревшим и отмечен для удаления". Если это так то как выполнить ТЗ finalize
//        System.out.println("Deleting Pet object: " + name);
//    }
//    protected void finalize() throws Throwable {
//        System.out.println("Deleting Human object: " + name);
//        super.finalize();
//    }
//
//}