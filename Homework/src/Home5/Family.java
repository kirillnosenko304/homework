//package Home5;
//
//import java.util.Arrays;
//
//public class Family extends Human implements AutoCloseable {
//    private String mother, father;
//    public Human[] getChildren() {
//        return children;
//    }
//    private Human[] children;
//    private int originalChildrenLength;
//    private Pet pet;    private Family family;
//    public Family getFamily() {
//        return family;
//    }
//    public void setPet(Pet pet) {
//        this.pet = pet;
//    }
//    public void setFamily(Family family) {
//        this.family = family;
//    }
//    @Override
//    public String toString() {
//        return "Family{\n" +
//                "  mother=" + mother + ",\n" +
//                "  father=" + father + ",\n" +
//                "  children=" + Arrays.toString(children) + ",\n" +
//                "  pet=" + pet + "\n" +
//                "}";
//    }
//    String allToString(Human human, Pet pet, Family family) {
//        return human.toString() + pet.toString() + family.toString();
//    }
//    public void addChild(Human child, String nameChild) {
//        child.setName(nameChild);
//        child.setFamily(this);
//        Human[] updaedChildren = Arrays.copyOf(children, children.length+1);
//        updaedChildren[updaedChildren.length-1]= child;
//        children=updaedChildren;
//        //Перевірка, що масив children збільшуеться
//        if (children.length == originalChildrenLength + 1 && children[children.length - 1] == child) {
//            System.out.println("Масив children збільшується на один елемент, яким є переданий об'єкт");
//        }
//    }
//    public void deleteChild(Family child) {
//        boolean childRemoved = false;
//        for (int i = 0; i < children.length; i++) {
//            if (children[i] == child) { // перевірка на еквівалентність об'єктів
//                children[i] = null; // видалення дитини з масиву
//                childRemoved = true;
//                break;
//            }
//        }
//        if (!childRemoved) {
//            System.out.println("Дитина не знайдена в масиві children");
//        }
//        // перевірка, що масив children залишається без змін
//        boolean childrenUnchanged = true;
//        for (Human childElement : children) {
//            if (childElement == child) { // перевірка на еквівалентність об'єктів
//                childrenUnchanged = false;
//                break;
//            }
//        }
//        if (!childrenUnchanged) {
//            System.out.println("Масив children змінився");
//        }
//    }
//    public int countFamily() {
//        return 2 + children.length; // 2 for mother and father
//    }
//
//    public Family(String mother, String father, Pet pet) {
//        if (mother != null && !mother.isEmpty() && father != null && !father.isEmpty()) {
//            this.mother = mother;
//            this.father = father;
//            setPet(pet);
//            this.children = new Human[0];
//
//        } else {
//            System.out.println("Не удалось создать семью. Необходимо указать обоих родителей.");
//        }
//    }
//    public Family(String mother, String father, int year, int iq) {
//        if (mother != null && !mother.isEmpty() && father != null && !father.isEmpty()) {
//            this.mother = mother;
//            this.father = father;
//            this.children = new Human[0];
//            setYear(year);
//            setIq(iq);
//
//        } else {
//            System.out.println("Не удалось создать семью. Необходимо указать обоих родителей.");
//        }
//    }
//    @Override
//    public void close() throws Exception {  // "Начиная с Java 9, метод finalize() в классе Object является устаревшим и отмечен для удаления". Если это так то как выполнить ТЗ finalize
//        System.out.println("Deleting Human object: " + this.getName());
//    }
//    protected void finalize() throws Throwable {
//        System.out.println("Deleting Pet object: " + family.getFamily());
//        super.finalize();
//    }
//}