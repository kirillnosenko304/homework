//package Home5;
//
//import java.util.Arrays;
//import java.util.Scanner;
//enum Species {
//    CAT("Кошка"),
//    DOG("Собака"),
//    LION("Лев"),
//    TIGER("Тигр"),
//    RABBIT("Заяц"),
//    WOLF("Волк"),
//    FOX("Лиса"),
//    DEER("Олень"),
//    GIRAFFE("Жираф"),
//    ELEPHANT("Слон"),
//    CUSTOM("Інший вид");
//
//    private final String name;
//    Species(String name) {
//        this.name = name;
//    }
//    public String getName() {
//        return name;
//    }
//}
//public class Pet implements AutoCloseable{
//    private String species, nickname;
//    private int age,trickLevel;
//    private String[] habits;
//    private String name;
//
//
//    public void setSpecies(String species) {
//        this.species = species;
//    }
//    public void setNickname(String nickname) {
//        this.nickname = nickname;
//    }
//    public void setAge(int age) {
//        this.age = age;
//    }
//    public void setTrickLevel(int trickLevel) {
//        this.trickLevel = trickLevel;
//    }
//    public String getSpecies() {
//        return species;
//    }
//    public String getNickname() {
//        return nickname;
//    }
//    public int getAge() {
//        return age;
//    }
//    public int getTrickLevel() {
//        return trickLevel;
//    }
//    public String[] getHabits() {
//        return habits;
//    }
//    public void setHabits(String[] habits) {
//        this.habits = habits;
//    }
//    @Override
//    public String toString() {
//        return "Pet{" +
//                "species='" + species + '\'' +
//                ", nickname='" + nickname + '\'' +
//                ", age=" + age +
//                ", trickLevel=" + trickLevel +
//                ", habits=" + Arrays.toString(habits) +
//                '}';
//    }
//    public void printPet(){
//        System.out.printf("Привіт, хазяїн, знову забув про мене? Це я - %s. Я - %s , мені - %d, я хитра тварина - %d%n",nickname,species,  age, trickLevel);
//    }
//    Pet(Species species, String nickname, int age, int trickLevel, String[] habits){
//        setSpecies(String.valueOf(species));
//        setNickname(nickname);
//        setAge(age);
//        setTrickLevel(trickLevel);
//        this.habits = habits;
//        System.out.printf("Це я - %s. Я - %s , мені - %d, я хитра тварина - %d", nickname ,species,  age, trickLevel);
//    }
//    Pet(){
//        System.out.println("Конструктор Pet");
//    }
//    Pet(String species,String nickname ){
//        setSpecies(species);
//        setNickname(nickname);
//        System.out.println(species + " " + nickname);
//    }
//    private String[] addHabits(String[] habits){
//        Scanner scanner = new Scanner(System.in);
//
//        return habits;
//    }
//    private void eat(){
//        System.out.println("Я їм");
//    }
//    private void respond (){
//        System.out.printf("Привіт, хазяїн. Я - %s. Я скучив!%n", this.nickname);
//    }
//    private void foul(){
//        System.out.printf("(%S) Потрібно добре замести сліди...%n", this.nickname);
//    }
//
//    @Override
//    public void close() throws Exception {  // "Начиная с Java 9, метод finalize() в классе Object является устаревшим и отмечен для удаления". Если это так то как выполнить ТЗ finalize
//        System.out.println("Deleting Pet object: " + name);
//    }
//    @Override
//    protected void finalize() throws Throwable {
//        System.out.println("Deleting Pet object: " + name);
//        super.finalize();
//    }
//}
