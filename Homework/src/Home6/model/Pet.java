package Home6.model;

import java.util.Arrays;
import java.util.Scanner;

enum Species {
    UNKNOWN("Невідомий"),
    CAT("Кошка"),
    DOG("Собака"),
    LION("Лев"),
    TIGER("Тигр"),
    RABBIT("Заяц"),
    WOLF("Волк"),
    FOX("Лиса"),
    DEER("Олень"),
    GIRAFFE("Жираф"),
    ELEPHANT("Слон"),
    CUSTOM("Інший вид"),
    ROBOCAT("РобоКіт");


    private final String name;

    Species(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
    public static Species getSpeciesByValue(String value) {
        for (Species species : Species.values()) {
            if (species.name.equalsIgnoreCase(value)) {
                return species;
            }
        }
        return UNKNOWN;
    }
}

public abstract class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }

    @Override
    public String toString() {
        return "Pet{" +
                "species='" + species + '\'' +
                ", nickname='" + nickname + '\'' +
                ", age=" + age +
                ", trickLevel=" + trickLevel +
                ", habits=" + Arrays.toString(habits) +
                '}';
    }

    public void printPet() {
        System.out.printf("\nПривіт, хазяїн, знову забув про мене? Це я - %s. Я - %s , мені - %d, я хитра тварина - %d%n\n", nickname, species, age, trickLevel);
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        setSpecies(String.valueOf(species));
        setNickname(nickname);
        setAge(age);
        setTrickLevel(trickLevel);
        this.habits = habits;
        System.out.printf("Це я - %s. Я - %s , мені - %d, я хитра тварина - %d", nickname, species, age, trickLevel);
    }

    public Pet() {
        System.out.println("Конструктор Pet");
    }

    public Pet(String species, String nickname) {
        setSpecies(species);
        setNickname(nickname);
        System.out.println(species + " " + nickname);
    }

    protected String[] addHabits(String[] habits) {
        Scanner scanner = new Scanner(System.in);

        return habits;
    }

    protected abstract void eat();

    protected abstract void respond();

    protected void foul() {
        System.out.printf("(%S) Потрібно добре замести сліди...%n", this.nickname);
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Deleting Pet object: " + nickname);
        super.finalize();
    }
}

class Cat extends Pet implements Fouling {

    public Cat(String nickname) {
        super(String.valueOf(Species.CAT), nickname, 0, 0, null);
    }

    public Cat(String species, String nickname, int age, int tricklevel, String[] strings) {
        setSpecies(species);setNickname(nickname);setAge(age);setTrickLevel(tricklevel);setHabits(strings);
    }

    @Override
    protected void eat() {
        System.out.println("Кошка їсть рибу");
    }

    @Override
    protected void respond() {
        System.out.println("Мяу!");
    }

    @Override
    public void foul() {
        System.out.printf("(%s) Потрібно добре замести сліди...%n", getNickname());
    }
}

class Fish extends Pet {
    public Fish(String nickname) {
        super(String.valueOf(Species.CUSTOM), nickname, 0, 0, null);
    }

    @Override
    protected void eat() {
        System.out.println("Рибка їсть планктон");
    }

    @Override
    protected void respond() {
        // Fish does not respond
    }
}

class Dog extends Pet implements Fouling {
    public Dog(String nickname) {
        super(String.valueOf(Species.DOG), nickname, 0, 0, null);
    }

    @Override
    protected void eat() {
        System.out.println("Собака їсть корм");
    }

    @Override
    protected void respond() {
        System.out.println("Гав!");
    }

    @Override
    public void foul() {
        System.out.printf("(%s) Потрібно прибрати за собакою...%n", getNickname());
    }
}

