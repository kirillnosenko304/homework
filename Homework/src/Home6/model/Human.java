package Home6.model;

import Home6.taskPlanner_hm4;

import java.util.Arrays;

enum WorkSchedule {
    PROGRAMMER("Программист"),
    DOCTOR("Врач"),
    TEACHER("Учитель"),
    ENGINEER("Инженер"),
    ARTIST("Художник"),
    ATHLETE("Спортсмен"),
    MUSICIAN("Музыкант"),
    WRITER("Писатель"),
    CHEF("Повар"),
    SCIENTIST("Ученый");

    private final String occupation;

    WorkSchedule(String occupation) {
        this.occupation = occupation;
    }

    public String getOccupation() {
        return occupation;
    }
}

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private Home6.taskPlanner_hm4 taskPlanner_hm4;

    public Human(String name, String surname, int year, int iq) {
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
    }

    public String toString(Pet pet) {
        return "Человек{" +
                "имя='" + name + '\'' +
                ", фамилия='" + surname + '\'' +
                ", год рождения=" + year +
                ", IQ=" + iq +
                '}' + "Питомец{" +
                "вид='" + pet.getSpecies() + '\'' +
                ", кличка='" + pet.getNickname() + '\'' +
                ", возраст=" + pet.getAge() +
                ", уровень трюков=" + pet.getTrickLevel() +
                ", привычки=" + Arrays.toString(pet.getHabits()) +
                '}';
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    Human() {
        System.out.println("Конструктор Human");
    }

    public void greetPet() {
        System.out.println("Привет, приятель!");
    }
    protected void describePet(Pet pet) {
        System.out.printf("У меня есть %s, ей %d лет, она %s\n", pet.getSpecies(), pet.getAge(), threshold(pet));
    }

    protected void taskPlanner() {
        Home6.taskPlanner_hm4 taskPlan = new taskPlanner_hm4();
    }

    private String threshold(Pet pet) {
        if (pet.getTrickLevel() >= 50) {
            return "очень хитрая";
        } else {
            return "почти не хитрая";
        }
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Удаление объекта Human: " + name);
        super.finalize();
    }
}

