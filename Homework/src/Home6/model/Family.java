package Home6.model;

import java.io.Serializable;
import java.util.Arrays;

public class Family extends Human implements Serializable {
    private String mother;
    private String father;
    private Human[] children;
    private Pet pet;
    private Family family;
    private int originalChildrenLength;

    public Human[] getChildren() {
        return children;
    }

    public Family getFamily() {
        return family;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    @Override
    public String toString() {
        return "Family{\n" +
                "  mother='" + mother + "',\n" +
                "  father='" + father + "',\n" +
                "  children=" + Arrays.toString(children) + ",\n" +
                "  pet=" + pet + "\n" +
                "}";
    }

    public String allToString(Human human, Pet pet, Family family) {
        return human.toString() + pet.toString() + family.toString();
    }

    public void addChild(Human child, String nameChild) {
        child.setName(nameChild);
        child.setFamily(this);
        Human[] updatedChildren = Arrays.copyOf(children, children.length + 1);
        updatedChildren[updatedChildren.length - 1] = child;
        children = updatedChildren;

        // Проверка, что массив children увеличивается
        if (children.length == originalChildrenLength + 1 && children[children.length - 1] == child) {
            System.out.println("Массив children увеличивается на один элемент, которым является переданный объект");
        }
    }

    public void deleteChild(Human child) {
        boolean childRemoved = false;
        for (int i = 0; i < children.length; i++) {
            if (children[i] == child) {
                children[i] = null; // Удаление ребенка из массива
                childRemoved = true;
                break;
            }
        }
        if (!childRemoved) {
            System.out.println("Дитина не знайдена в масиві children");
        }

        // Проверка, что массив children остается без изменений
        boolean childrenUnchanged = true;
        for (Human childElement : children) {
            if (childElement == child) {
                childrenUnchanged = false;
                break;
            }
        }
        if (!childrenUnchanged) {
            System.out.println("Масив children змінився");
        }
    }

    public int countFamily() {
        return 2 + children.length; // 2 для матери и отца
    }

    public Family(String mother, String father, Pet pet) {
        if (mother != null && !mother.isEmpty() && father != null && !father.isEmpty()) {
            this.mother = mother;
            this.father = father;
            setPet(pet);
            this.children = new Human[0];
        } else {
            System.out.println("Не удалось создать семью. Необходимо указать обоих родителей.");
        }
    }

    public Family(String mother, String father, int year, int iq) {
        if (mother != null && !mother.isEmpty() && father != null && !father.isEmpty()) {
            this.mother = mother;
            this.father = father;
            this.children = new Human[0];
            setYear(year);
            setIq(iq);
        } else {
            System.out.println("Не удалось создать семью. Необходимо указать обоих родителей.");
        }
    }

    @Override
    protected void finalize() throws Throwable {
        System.out.println("Удаление объекта Pet: " + family.getFamily());
        super.finalize();
    }
}
