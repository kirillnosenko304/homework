package Home6.model;

public class RoboCat extends Pet {
    public RoboCat(String nickname, int age, int trickLevel, String[] habits) {
        super(String.valueOf(Species.ROBOCAT), nickname, age, trickLevel, habits);
    }

    @Override
    public void eat() {
        System.out.println("РобоКіт харчується спеціальним паливом.");
    }

    @Override
    public void respond() {
        System.out.println("Біп-боп! Я РобоКіт!");
    }
}
