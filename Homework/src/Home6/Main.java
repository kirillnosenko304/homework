package Home6;

import Home6.model.Couple;
import Home6.model.Family;
import Home6.model.Human;
import Home6.model.RoboCat;

import java.io.*;

public class Main {
    public static void main(String[] args) {

//        Pet pet = new Pet("Кошка", "Nick", 5, 43, new String[]{"test, dwasd"}) {
//            @Override
//            protected void eat() {
//
//            }
//            @Override
//            protected void respond() {
//
//            }
//        };
        RoboCat roboCat = new RoboCat("RoboKitty", 2, 5, new String[]{"Sleeping", "Playing"});
        roboCat.printPet();

        roboCat.eat();
        roboCat.respond();
        Couple couple = new Couple();
        Human child = couple.bornChild();

        Family family = new Family("nic" , "dwads", roboCat);
        System.out.println(family.toString());
    }
}
