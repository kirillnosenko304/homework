package Home2;

import java.util.Scanner;
public class lineshoot {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("count of Targets: ");
        int sizeofX = scanner.nextInt();
        char[][] gameBoard = createGameBoard(5, sizeofX); // Створення початкового ігрового поля

        System.out.println("All Set. Get ready to rumble!");

        int points = 0;
        boolean win = false; // Флаг, указывающий на победу
        while (true) {
            // Виведення поточного ігрового поля
            printGameBoard(gameBoard);

            //Введення лінії для стрільби
            int row;
            do {
                System.out.print("Enter the row (1-5): ");
                row = scanner.nextInt();
            } while (!isValidInput(row));

            // Введення стовпчика для стрільби
            int column;
            do {
                System.out.print("Enter the column (1-5): ");
                column = scanner.nextInt();
            } while (!isValidInput(column));

            // Проверка попадания
            boolean hit = checkHit(gameBoard, row-1, column-1);
            if (hit) {
                points++;
                System.out.println("Congratulations! You hit the target!");
                System.out.println(String.format("|points = %d| |Count of targets = %d|", points, sizeofX));
                if (points == sizeofX) {
                    win = true;
                    break; // Выход из цикла при достижении необходимого количества очков
                }
                updateGameBoard(gameBoard, row - 1, column - 1);
            } else {
                System.out.println("Missed. Try again!");
                updateGameBoard(gameBoard, row - 1, column - 1);}
        }
        // Вывод сообщения после окончания цикла в случае победы
        if (win) {
            System.out.println("You win!");
        }
    }
    // Метод для перевірки введеного числа на валідність
    private static boolean isValidInput(int number ) {
        return number >= 1 && number <= 5;
    }
    // Метод для створення початкового ігрового поля
    private static char[][] createGameBoard(int size, int sizeofX) {
        char[][] gameBoard = new char[size][size];
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                gameBoard[i][j] = '_'; // Порожня клітинка
            }
        }
        // Випадкове розміщення цілі
        for (int i = 0; i <sizeofX ; i++) {
            int targetRow = (int) (Math.random() * size);
            int targetColumn = (int) (Math.random() * size);
            gameBoard[targetRow][targetColumn] = 'x'; // Позначення цілі
        }
        return gameBoard;
    }
    // Метод для виведення ігрового поля
    private static void printGameBoard(char[][] gameBoard) {
        int size = gameBoard.length;
        System.out.println("0 | 1 | 2 | 3 | 4 | 5 |");
        for (int i = 0; i < size; i++) {
            System.out.print(i + 1 + " | ");
            for (int j = 0; j < size; j++) {
                System.out.print(gameBoard[i][j] + " | ");
            }
            System.out.println();
        }
    }
    // Метод для перевірки попадання
    private static boolean checkHit(char[][] gameBoard, int row, int column) {
        return gameBoard[row][column] == 'x';
    }
    private static void updateGameBoard(char[][] gameBoard, int row, int column) {
        if (gameBoard[row][column] == 'x') {
            gameBoard[row][column] = 'O'; // Попадание в цель
        } else if ((gameBoard[row][column] == 'O')){
            gameBoard[row][column] = 'O';
        }
        else gameBoard[row][column] = '*';
    }
}
