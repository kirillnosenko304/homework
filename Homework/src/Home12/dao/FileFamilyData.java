package Home12.dao;

import Home12.model.Family;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.*;
import java.lang.reflect.Type;
import java.util.List;

public class FileFamilyData {
    private final String filePath;
    private File file;

    public File getFile() {
        return file;
    }

    public void setFile(File file) {
        this.file = file;
    }
    public String getFilePath() {
        return filePath;
    }
    public FileFamilyData(String filePath) throws IOException {
        this.filePath = filePath;
        file = new File(filePath);
        if (!file.exists()) {
            file.createNewFile();
        }
    }

    public void checkFilePath() throws IOException {
        File file = new File(filePath);
        if (!file.exists()) {
            file.createNewFile();
        }
        System.out.println(file);
    }

    // Зберегти дані в файл
//    public void saveData(List<Family> families) {
//        try (ObjectOutputStream outputStream = new ObjectOutputStream(new FileOutputStream(filePath))) {
//            outputStream.writeObject(families);
//            System.out.println("Данные сохранены в файл: " + filePath);
//        } catch (IOException e) {
//            System.err.println("Ошибка при сохранении данных: " + e.getMessage());
//        }
//    }

    // Завантажити дані з файлу
//    public List<Family> loadData() {
//        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream(filePath))) {
//            List<Family> loadedFamilies = (List<Family>) inputStream.readObject();
//            System.out.println("Данные загружены из файла: " + filePath);
//            return loadedFamilies;
//        } catch (IOException | ClassNotFoundException e) {
//            System.err.println("Ошибка при загрузке данных: " + e.getMessage());
//            return null;
//        }
//    }
    // Метод для збереження даних у файл
    // Зберегти дані у файл у вигляді рядка
    public static void saveData(List<Family> families, String fileName) {
        try (BufferedWriter writer = new BufferedWriter(new FileWriter(fileName))) {
            Gson gson = new Gson();
            String jsonData = gson.toJson(families);
            writer.write(jsonData);
            System.out.println("Дані збережено в файл: " + fileName);
        } catch (IOException e) {
            System.err.println("Помилка збереження даних у файл: " + e.getMessage());
        }
    }

    // Метод для завантаження даних з файлу
    public static List<Family> loadData(String fileName) {
        try (BufferedReader reader = new BufferedReader(new FileReader(fileName))) {
            Gson gson = new Gson();
            Type familyListType = new TypeToken<List<Family>>() {}.getType();
            List<Family> families = gson.fromJson(reader, familyListType);
            System.out.println("Дані завантажено з файлу: " + fileName);
            return families;
        } catch (IOException e) {
            System.err.println("Помилка завантаження даних з файлу: " + e.getMessage());
            return null;
        }
    }
}



