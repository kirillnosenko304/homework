package Home12;

import Home12.controll.FamilyService;
import Home12.dao.FileFamilyData;

import java.io.IOException;

public class Main {
    public static void main(String[] args) {
//        FamilyDao familyDao = new FamilyService();
//
//        //Створення списку тварин
//        Set<String> habits = new HashSet<>();
//        habits.add("прыгать на стол");
//        habits.add("играть с мячиком");
//        Pet pet = new Pet("Dog", "Adam", habits) {
//            @Override
//            protected void eat() {
//            }
//            @Override
//            protected void respond() {
//            }
//        };
//
//        // mother and father
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
//
//
//        Human mother = new Human("Helen", "Samol", "Woman", "20/03/2016" ,190);
//        Human father = new Human("Steav", "Samol", "Man","20/03/2016" ,125);
//
//        Human mother1 = new Human("March", "Kononov", "Woman", "20/03/2016" ,190);
//        Human father1 = new Human("Homer", "Kononov", "Man", "20/03/2016" ,125);
//
//        // Створення Family
//        Family family = new Family(mother, father, Collections.singleton(pet));
//        Family family1 = new Family(mother1, father1);
//
//        //Создание детей
//        Human child1 = new Human("Kora", "Marks", "Woman", "20/03/2016", 144);
//
//
//        System.out.println(mother1.describeAge()); // Correct
//
//        familyDao.bornChild(family, "Dima"); // Рождение ребенка
//        familyDao.adoptChild(family1, child1);        // усыновление ребенка
//        // Вывод семей
//        System.out.println("Admin: Family One = " + family);
//        System.out.println("Admin: Family Two = " + family1);
//
//        // Отримання списку дітей з сім'ї
//        // List<Human> childrenList = family.getChildren();
//        // Виведення імен дітей
//        /*
//            for (Human child : childrenList) {
//                System.out.println(child);
//        }*/
//
//        System.out.println("FamilyController \n\n");
//        familyDao.addFamily(family);
//        familyDao.addFamily(family1);
//
//        //Вывод на экран всего семейства
//        familyDao.displayAllFamilies();
//        System.out.println("\n\n");
//
//        // Кількість сімей у списку
//        System.out.println( "Count of family = "+ familyDao.count());
//        //familyDao.deleteFamily(family1);
//        System.out.println(familyDao.getAllFamilies());
//
//        // Получаем семью по индексу
//        System.out.println("Method CheckIndexFamily = ");
//        int index = 1;
//        Optional<Family> familyByIndex = familyDao.getFamilyByIndex(index);
//
//        if (familyByIndex != null) {
//            System.out.println("Семья по индексу " + index + ": " + familyByIndex);
//        } else {
//            System.out.println("Семьи с индексом " + index + " не существует.");
//        }
//
//
//        //Удаление ребонка старше n лет
//        //familyDao.deleteAllChildrenOlderThen(4);
//        familyDao.displayAllFamilies();
        FamilyService familyService;
        //familyService.run();
        FileFamilyData fmd;

        try {
            fmd = new FileFamilyData("D:\\Projects\\Java\\DanIt\\Homework\\DataBase.txt");
            familyService = new FamilyService(fmd);
            fmd.checkFilePath();
            familyService.run();
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

    }

}
