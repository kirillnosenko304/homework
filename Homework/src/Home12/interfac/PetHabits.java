package Home12.interfac;

import java.util.Set;

public interface PetHabits {
    Set<String> getHabits();
    void setHabits(Set<String> habits);
}
