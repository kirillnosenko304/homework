package Home12.interfac;

import Home12.dao.Species;
import Home12.model.Pet;

import java.util.Set;

public interface Fouling {
    void foul();
}
class Cat extends Pet implements Fouling {

    public Cat(String nickname) {
        super(Species.valueOf(String.valueOf(Species.CAT)), nickname, 0, 0, null);
    }

    public Cat(String species, String nickname, int age, int trickLevel, Set<String> habits) {
        super(Species.valueOf(species), nickname, age, trickLevel, habits);
    }

    @Override
    protected void eat() {
        System.out.println("Кошка їсть рибу");
    }

    @Override
    protected void respond() {
        System.out.println("Мяу!");
    }

    @Override
    public void foul() {
        System.out.printf("(%s) Потрібно добре замести сліди...%n", getNickname());
    }
}
