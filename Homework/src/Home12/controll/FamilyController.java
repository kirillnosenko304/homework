package Home12.controll;

import Home12.interfac.FamilyDao;
import Home12.model.Family;

import java.util.List;

public class FamilyController {
    private final FamilyDao familyDao;

    public FamilyController(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }


}
